# Notes about the project


## The team

- CANTORE Andrea: generalekamikaze@gmail.com
- LIVI Sergio: sergio.livi@studio.unibo.it


## Comments

All the results are in the "results" directory.

For complexity calculations:
- N is the number of hosts (that is always 800, in our datacenter)
- M is the number of VMs to allocate (current day)

For the complexity we studied the worst case.


### Support for Highly-Available applications

> What is the temporal complexity of the algorithm?

For each VM to allocate, our algorithm cycles all the hosts and check the non-existence of affine VMs (that is a cycle on all the VMs instantiated on the host).
So the maximum complexity for the single allocation is:

    M-1 = O(M)

That's because, in the worst case, we are going to check the current VM with all the others.

> What is the impact of such an algorithm over the cluster hosting capacity?

In this case, the anti-affinity groups are made of 100 VMs. That means that we need at least 100 hosts.
In general we will need as many hosts as the cardinality of the biggest anti-affinity set.


### Balance the load

> What is the temporal complexity of the algorithm?

Our implementation relies on Java's `Collection.sort` algorithm. Our comparator is a simple comparison between `AvailableMips` on the hosts.
After that, the list of hosts is cycled to find a suitable one.
So complexity is:

    O(N log(N))

>  Establish a metric to describe the balancing, justify you choice, and observe its variation depending on the underlying scheduler.

Our metric is based on the variance of the ratio between `AvailableMips` and `TotalMips` of each host. This value is multiplied for a constant (`1000`) for easier reading. Perfect balance should be at `0`, while greater values denote unbalancement.
We then collected the average value and the peak, across the entire day.


### Get rid of SLA violations

> What is the temporal complexity of the algorithm?

The scheduler we propose aims to give each VM the computing power that it can ask at peak utilization. This can be done by looking at VMs already allocated on the machine, and summing the requested MIPS.
The trap is that each VM is asking one PE, while hosts have two, and the single VM will run only on one PE at a time.
That means that the calculation of free MIPS has to be done for every PE, and not for the entire host.
The complexity is:

    O(M)

We basically do the same cycles as in `antiAffinity` scheduler. The only complexity difference is that we loop also on PEs number. Our datacenter have only 2-PEs host machines, so we can treat it as a constant and leave it out of complexity computation.


### Energy-efficient schedulers

#### static version

> What is the temporal complexity of the algorithm?

In this case, for robustness purposes we decided to use `Collection.sort` as in `balance` scheduler.
The `Comparator` simply reads attributes of `Host` class, so the complexity is:

    O(N log(N))

#### dynamic version

In order to create a new dynamic scheduler, increasing the total revenue, we adapted the static version of our
energy efficient scheduler, in order to balance the virtual machines in the hosts thanks to the migration operation.
The result is a very efficient scheduler in which the used machines are minimum and the energy consumption is 
less thanks to a better balance of the resources and minimum penalties.
N.B. The result could be better with a greater CPUs overbooking, but seems that cloudsim locks an obsessive stress
of the CPUs.

### Greedy schedulers

The greedy scheduler is an extension of the NoViolation scheduler in which we try to minimize the number of used
machine. The Optimizator iterate on the vm sorted for requested ram and on the hosts sorted by available Ram,
skipping the vm in full host (hostRam < 10) and the unused host. The result is an optimized scheduler in which the
revenue are are greatest, the penalties are negligible, the hosts are balanced and the energy consumption is smallest.
We have to add to the SLA violations the following complexity:

    O(M * N)
