package fr.unice.vicc;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.SimEntity;
import org.cloudbus.cloudsim.core.SimEvent;
import org.cloudbus.cloudsim.power.PowerHost;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class HostsObserver extends SimEntity {

	/** The custom event id, must be unique. */
	public static final int OBSERVE = 425161;

	private List<PowerHost> hosts;

	private int[] usedMachinesPeaks;
	private int[] machinesNumber;

	private float delay;

	public static final float DEFAULT_DELAY = 1;

	public HostsObserver(List<PowerHost> hosts) {
		this(hosts, DEFAULT_DELAY);
	}

	public HostsObserver(List<PowerHost> hosts, float delay) {
		super("BalanceObserver");
		this.hosts = hosts;
		this.delay = delay;
	}

	private int getHostType(PowerHost h) {
		for (int i = 0; i < Constants.HOST_TYPES; i++)
			if (h.getTotalMips() == Constants.HOST_MIPS[i] * h.getPeList().size() && h.getRam() == Constants.HOST_RAM[i] && h.getPeList().size() == Constants.HOST_PES[i])
				return i;
		throw new IllegalArgumentException();
	}

	@Override
	public void processEvent(SimEvent ev) {
		switch (ev.getTag()) {
		case OBSERVE:
			int[] usedMachines = new int[Constants.HOST_TYPES];
			for (int i = 0; i < Constants.HOST_TYPES; i++)
				usedMachines[i] = 0;
			for (PowerHost h : hosts) {
				if (h.getRamProvisioner().getUsedRam() > 0) {
					int type = getHostType(h);
					usedMachines[type]++;
				}
			}
			for (int i = 0; i < Constants.HOST_TYPES; i++) {
				if (usedMachines[i] > usedMachinesPeaks[i])
					usedMachinesPeaks[i] = usedMachines[i];
			}
			send(this.getId(), delay, OBSERVE, null);
		}
	}

	@Override
	public void shutdownEntity() {
		Log.printLine(getName() + " is shutting down...");
		System.out.println("Used physical machines stats:");
		for (int i = 0; i < Constants.HOST_TYPES; i++) {
			System.out.println("" + Constants.HOST_MIPS[i] + ": " + usedMachinesPeaks[i] + " / " + machinesNumber[i]);
		}
	}

	@Override
	public void startEntity() {
		Log.printLine(getName() + " is starting...");

		usedMachinesPeaks = new int[Constants.HOST_TYPES];
		for (int i = 0; i < Constants.HOST_TYPES; i++)
			usedMachinesPeaks[i] = Integer.MIN_VALUE;

		machinesNumber = new int[Constants.HOST_TYPES];
		for (int i = 0; i < Constants.HOST_TYPES; i++)
			machinesNumber[i] = 0;
		for (PowerHost h : hosts) {
			int type = getHostType(h);
			machinesNumber[type]++;
		}

		send(this.getId(), delay, OBSERVE, null);
	}
}
