package fr.unice.vicc;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DynEnergyVmAllocationPolicy extends StatEnergyVmAllocationPolicy {
	public DynEnergyVmAllocationPolicy(List<? extends Host> list) {
		super(list);
	}
	
	@Override
	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vms) {
		List<Host> usedHosts = super.getHostList();
		
		List<Map<String, Object>> map = new ArrayList<>();
		for (Vm v : vms) {
			
			if(v.getHost().getRamProvisioner().getUsedRam()< 10)
				continue;
			
			for (Host h : usedHosts) {
				
				if (!v.getHost().equals(h)
							&& h.isSuitableForVm(v)) {
					Map<String, Object> m = new HashMap<>();
					m.put("vm", v);
					m.put("host", h);
					map.add(m);
					usedHosts.remove(h);
					break;
				}
			}
		}
		return map;
	}

}
