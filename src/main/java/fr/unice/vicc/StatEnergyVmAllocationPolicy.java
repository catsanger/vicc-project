package fr.unice.vicc;

import org.cloudbus.cloudsim.Host;

import java.util.Collections;
import java.util.List;

public class StatEnergyVmAllocationPolicy extends NaiveVmAllocationPolicy {

	public StatEnergyVmAllocationPolicy(List<? extends Host> list) {
		super(list);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends Host> List<T> getHostList() {
		List<Host> list = super.getHostList();
		Collections.sort(list, new HostsTotalMipsAscAvailableRamDescComparator());
		return (List<T>) list;
	}

}
