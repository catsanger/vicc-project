package fr.unice.vicc;

import java.util.Comparator;

import org.cloudbus.cloudsim.Host;

public class HostsAvailableMipsDescendingComparator implements Comparator<Host> {
	@Override
	public int compare(Host h1, Host h2) {
		return (int) (h2.getAvailableMips() - h1.getAvailableMips());
	}
}
