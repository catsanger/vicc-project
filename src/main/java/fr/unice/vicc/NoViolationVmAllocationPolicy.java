package fr.unice.vicc;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

import java.util.List;

public class NoViolationVmAllocationPolicy extends NaiveVmAllocationPolicy {

	public NoViolationVmAllocationPolicy(List<? extends Host> list) {
		super(list);
	}

	public int[] remainingPesMips(Host h) {
		int[] remainingPesMips = new int[h.getPeList().size()];

		for (int i = 0; i < h.getPeList().size(); i++) {
			remainingPesMips[i] = h.getPeList().get(i).getMips();
		}

		for (Vm v : h.getVmList()) {
			for (int i = 0; i < remainingPesMips.length; i++) {
				if (v.getMips() <= remainingPesMips[i]) {
					// we consider only one-pe VMs
					remainingPesMips[i] -= v.getMips();
					break;
				}
			}
		}
		return remainingPesMips;
	}

	public boolean allocateHostForVm(Vm vm) {
		for (Host h : getHostList()) {
			for (int mips : remainingPesMips(h)) {
				if (vm.getMips() < mips && h.vmCreate(vm)) {
					vmTable.put(vm.getUid(), h);
					return true;
				}
			}
		}
		// Ok, there's no room for this VM without violating SLA. We are sad, we
		// go randomly.
		for (Host h : getHostList()) {
			if (h.vmCreate(vm)) {
				vmTable.put(vm.getUid(), h);
				return true;
			}
		}
		return false;
	}
}
