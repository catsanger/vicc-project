package fr.unice.vicc;

import java.util.Comparator;

import org.cloudbus.cloudsim.Vm;


public class VmTotalRamDescComparator implements Comparator<Vm> {
	@Override
	public int compare(Vm v1, Vm v2) {
		return v2.getRam()
				- v1.getRam();
	}
}
