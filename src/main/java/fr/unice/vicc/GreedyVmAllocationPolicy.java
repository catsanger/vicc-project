package fr.unice.vicc;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GreedyVmAllocationPolicy extends NoViolationVmAllocationPolicy {
	public GreedyVmAllocationPolicy(List<? extends Host> list) {
		super(list);
	}
	
	@Override
	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vms) {
		List<Host> usedHosts = super.getHostList();
		Collections.sort(usedHosts, new HostsTotalRamDescComparator());	
		List<Vm> sortedVm = (List<Vm>) vms;
		Collections.sort( sortedVm, new VmTotalRamDescComparator());	


		List<Map<String, Object>> map = new ArrayList<>();
		for (Vm v : sortedVm) {
			
			if(v.getHost().getRamProvisioner().getUsedRam()< 10)
				continue;
			
			for (Host h : usedHosts) {
				if(h.getRamProvisioner().getUsedRam() == 0)
					continue;
				
				if (!v.getHost().equals(h)
							&& h.isSuitableForVm(v)) {
					Map<String, Object> m = new HashMap<>();
					m.put("vm", v);
					m.put("host", h);
					map.add(m);
					usedHosts.remove(h);
					break;
				}
			}
		}
		return map;
	}
}
