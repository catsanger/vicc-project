package fr.unice.vicc;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.SimEntity;
import org.cloudbus.cloudsim.core.SimEvent;
import org.cloudbus.cloudsim.power.PowerHost;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class BalanceObserver extends SimEntity {

	/** The custom event id, must be unique. */
	public static final int OBSERVE = 628178;

	private List<PowerHost> hosts;

	private int count;
	private double sum;
	private double peak;

	private float delay;

	public static final float DEFAULT_DELAY = 1;

	public BalanceObserver(List<PowerHost> hosts) {
		this(hosts, DEFAULT_DELAY);
	}

	public BalanceObserver(List<PowerHost> hosts, float delay) {
		super("BalanceObserver");
		this.hosts = hosts;
		this.delay = delay;
	}

	/**
	 * Get the datacenter instantaneous balance. Basically it is obtained by the
	 * variance of the number of available MIPS on each host
	 * 
	 * @return 0 if the datacenter is balanced, otherwise more
	 */
	private double getAvailableMipsVariance() {
		int i = 0, n = hosts.size();
		double[] mips = new double[n];
		double sum = 0;
		for (PowerHost h : hosts) {
			mips[i] = h.getAvailableMips() / h.getTotalMips();
			sum += mips[i];
			i++;
		}

		double avg = sum / n;

		double tmp = 0;
		for (double x : mips)
			tmp += (avg - x) * (avg - x);

		return 1000 * tmp / n;
	}

	/*
	 * This is the central method to implement. CloudSim is event-based. This
	 * method is called when there is an event to deal in that object. In
	 * practice: create a custom event (here it is called OBSERVE) with a unique
	 * int value and deal with it.
	 */
	@Override
	public void processEvent(SimEvent ev) {
		// I received an event
		switch (ev.getTag()) {
		case OBSERVE: // It is my custom event
			// I must observe the datacenter
			count++;
			double cur = getAvailableMipsVariance();
			if (cur > peak)
				peak = cur;
			sum += cur;
			// System.out.println("Current balance value: " + cur);
			// Observation loop, re-observe in `delay` seconds
			send(this.getId(), delay, OBSERVE, null);
		}
	}

	/**
	 * Get the average balance.
	 * 
	 * @return 0 if the datacenter is balanced, otherwise more
	 */
	public double getAverage() {
		return sum / count;
	}

	/**
	 * Get the peak balance.
	 * 
	 * @return 0 if the datacenter is balanced, otherwise more
	 */
	public double getPeak() {
		return peak;
	}

	@Override
	public void shutdownEntity() {
		Log.printLine(getName() + " is shutting down...");
		System.out.println("Balance:    " + String.format("%.1f", getAverage()) + "; Peak: " + String.format("%.1f", getPeak()));
	}

	@Override
	public void startEntity() {
		Log.printLine(getName() + " is starting...");
		// I send to myself an event that will be processed in `delay` second by
		// the method
		// `processEvent`
		count = 0;
		sum = 0;
		send(this.getId(), delay, OBSERVE, null);
	}
}
