package fr.unice.vicc;

import java.util.Comparator;

import org.cloudbus.cloudsim.Host;

public class HostsTotalMipsAscAvailableRamDescComparator implements Comparator<Host> {
	@Override
	public int compare(Host h1, Host h2) {
		int mipsCompare = h1.getTotalMips() - h2.getTotalMips();
		if (mipsCompare != 0)
			return mipsCompare;
		return h2.getRamProvisioner().getUsedRam() / h2.getRam()
				- h1.getRamProvisioner().getUsedRam() / h1.getRam();
	}
}
