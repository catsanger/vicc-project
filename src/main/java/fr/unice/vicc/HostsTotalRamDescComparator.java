package fr.unice.vicc;

import java.util.Comparator;

import org.cloudbus.cloudsim.Host;

public class HostsTotalRamDescComparator implements Comparator<Host> {
	@Override
	public int compare(Host h1, Host h2) {
		return h2.getRamProvisioner().getUsedRam() / h2.getRam()
				- h1.getRamProvisioner().getUsedRam() / h1.getRam();
	}
}
