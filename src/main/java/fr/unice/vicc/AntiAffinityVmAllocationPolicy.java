package fr.unice.vicc;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

import java.util.List;

public class AntiAffinityVmAllocationPolicy extends NaiveVmAllocationPolicy {

	public AntiAffinityVmAllocationPolicy(List<? extends Host> list) {
		super(list);
	}

	private int getVmGroup(Vm vm) {
		return vm.getId() - (vm.getId() % 100);
	}

	private boolean isAffine(Vm vm, Host host) {
		int vmGroup = getVmGroup(vm);
		for (Vm v : host.getVmList()) {
			if (vmGroup == getVmGroup(v))
				return true;
		}
		return false;
	}

	public boolean allocateHostForVm(Vm vm, Host host) {
		if (!isAffine(vm, host) && host.vmCreate(vm)) {
			vmTable.put(vm.getUid(), host);
			return true;
		}
		return false;
	}

	public boolean allocateHostForVm(Vm vm) {
		for (Host h : getHostList()) {
			if (!isAffine(vm, h) && h.vmCreate(vm)) {
				vmTable.put(vm.getUid(), h);
				return true;
			}
		}
		return false;
	}
}
