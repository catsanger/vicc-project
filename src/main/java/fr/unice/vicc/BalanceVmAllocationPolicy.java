package fr.unice.vicc;

import org.cloudbus.cloudsim.Host;

import java.util.Collections;
import java.util.List;

public class BalanceVmAllocationPolicy extends NaiveVmAllocationPolicy {
	public BalanceVmAllocationPolicy(List<? extends Host> list) {
		super(list);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends Host> List<T> getHostList() {
		List<Host> list = super.getHostList();
		Collections.sort(list, new HostsAvailableMipsDescendingComparator());
		return (List<T>) list;
	}
}
